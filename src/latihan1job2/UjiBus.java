/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package latihan1job2;

/**
 *
 * @author ASUS
 */
public class UjiBus {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //membuat objek
        Bus busMini = new Bus();
        //memasukkan nilai penumpang dan maksimum
        busMini.penumpang = 5;
        busMini.maxPenumpang = 15;
        //memanggil cetak()
        busMini.cetak();
        //menambahkan penumpang dan mencetak
        busMini.penumpang = busMini.penumpang +5;
        busMini.cetak();
        
        //mengurangi jumlah penumpang dan mencetak
        busMini.penumpang = busMini.penumpang - 2;
        busMini.cetak();
        
        //menambahkan jumlah penumpang pada busmini
        busMini.penumpang = busMini.penumpang + 8;
        busMini.cetak();
        
    }
    
}
